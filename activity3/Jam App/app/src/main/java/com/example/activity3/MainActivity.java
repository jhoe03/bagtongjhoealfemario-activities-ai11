package com.example.activity3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText Input = findViewById(R.id.userInput);
        Button Show = findViewById(R.id.button);

        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Text = Input.getText().toString();
                if (Text.isEmpty()) {
                    alert(" ");
                } else {
                    alert(Text);
                }
            }
        });
    }

    private void alert(String message) {
        AlertDialog mydialog = new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        mydialog.show();
    }
}
