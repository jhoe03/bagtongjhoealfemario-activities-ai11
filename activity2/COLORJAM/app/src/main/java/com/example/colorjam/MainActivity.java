package com.example.colorjam;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    View screenView;
    Button changeColor;
    int[] color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        color = new int[] {Color.RED, Color. GREEN, Color. BLUE, Color. BLACK, Color. YELLOW, Color. GRAY, Color. CYAN, Color. WHITE, Color. LTGRAY, Color. DKGRAY, Color. TRANSPARENT};

        screenView = findViewById(R.id.rView);
        changeColor =(Button)findViewById(R.id.button);

        changeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int arycolor = color.length;

                Random random = new Random();
                int rColor = random.nextInt(arycolor);

                screenView.setBackgroundColor(color[rColor]);
            }
        });


    }
}