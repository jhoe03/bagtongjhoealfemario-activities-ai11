package myPackages;
import java.util.*;

public class Books{   
    protected  int[] ISBN = new int[50];
    protected String[] bTitle = new String[50];
    protected String[] bAuthor = new String[50];
    private int n;
    
        public int Books() {
        Scanner myS = new Scanner(System.in);
        System.out.println("[]Array Size is 50[]");
        System.out.print("How many books do you want to enter? ");
        n = myS.nextInt();
        return n;
    }
    public void IsbnAdder() {
        if(n < 51) {
        Scanner myS1 = new Scanner(System.in);
        Scanner myS2 = new Scanner(System.in);
        Scanner myS3 = new Scanner(System.in);
            
               int count = 1;
                for(int i=0; i<n;i++) {
                    System.out.println("\n" + "Book count " + count);
                        count = count+1;
                    System.out.print("Enter ISBN: ");
                        ISBN[i] = myS1.nextInt();
                    System.out.print("Enter Book Title: ");
                        bTitle[i] = myS2.nextLine();
                    System.out.print("Enter Book Author: ");
                        bAuthor[i] = myS3.nextLine();
                }
                System.out.print("\n");
                DisplayArr();
                
        } else {
            System.out.println("\n" + "Array size not enough!");
        }
    }
    public void DisplayArr() {
        int j=0;
        while(j<n) {
            System.out.println("==========================================");
            System.out.println("ISBN: " + ISBN[j] 
                    + "\n" + "Title: " + bTitle[j] 
                    + "\n" +"Author: "+bAuthor[j]);
            j++;
        }
    }
}//END\\ //Author @ Jhoe Alfe Mario S. Bagtong - AI24

//Book ISBN, Title and Author sample was retrieved from http://www.travelman.co.uk/list_of_titles.htm